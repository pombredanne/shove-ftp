#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''setup for shove-ftp'''

from os import getcwd
from os.path import join

from setuptools import setup, find_packages


def getversion(fname):
    '''Get the __version__ without importing.'''
    with open(fname) as f:
        for line in f:
            if line.startswith('__version__'):
                return '%s.%s.%s' % eval(line[13:].rstrip())

install_requires = list(l.strip() for l in open(
    join(getcwd(), 'reqs/requires.txt'),
))

setup(
    name='shove_ftp',
    version=getversion('shove_ftp/__init__.py'),
    description='Common object storage frontend',
    long_description=open('README.rst').read(),
    author='L. C. Rees',
    author_email='lcrees@gmail.com',
    url='https://bitbucket.org/lcrees/shove_ftp/',
    license='BSD',
    packages=find_packages(),
    test_suite='shove_ftp.tests',
    install_requires=install_requires,
    zip_safe=False,
    keywords='object storage persistence database ftp',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Database :: Front-Ends',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    ],
    entry_points='''
    [shove.stores]
    ftp=shove_ftp.store:FTPStore
    ''',
)
