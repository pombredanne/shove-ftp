from stuf.six import unittest
from tests.test_store import Store

@unittest.skip('reason')
class TestFTPStore(Store, unittest.TestCase):

    initstring = 'ftp://127.0.0.1/'

    def setUp(self):
        from shove import Shove
        self.store = Shove(self.initstring, compress=True)

    def tearDown(self):
        self.store.clear()
        self.store.close()